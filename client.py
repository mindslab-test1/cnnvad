import grpc
from vad_pb2 import Segment, WavFileBinary, Speech
from vad_pb2_grpc import VadStub
import wave 
import librosa as rosa
from scipy.io.wavfile import write
import numpy as np
CHUNK_SIZE = 1024*1024

def read_bytes(file_, num_bytes):
    while True:
        bin = file_.read(num_bytes)
        if len(bin) == 0:
            break
        yield bin

def read_test_file(fp):
    with open(fp, 'rb') as f:
        for byte in read_bytes(f,CHUNK_SIZE):
            yield WavFileBinary(bin=byte)

def read_test_file_pcm(fp):
    with wave.open(fp, 'rb') as rf:
        while True:
            data = rf.readframes(CHUNK_SIZE)
            if len(data) == 0:
                break
            yield Speech(bin = data)
if __name__ == "__main__":
    with grpc.insecure_channel("127.0.0.1:5002") as channel:
        stub = VadStub(channel)
        testfile = 'goodsen1_mono.wav'
        wav,_ = rosa.load(testfile, sr =16000)
        wavwav =np.array([])
        pcmwav =np.array([])
        print('WAV')
        outputs=stub.DetectWav(read_test_file(testfile))
        for seg in outputs:
            print(seg.start, seg.end)
            wavwav = np.concatenate((wavwav, wav[seg.start:seg.end]))
        print('PCM')
        outputs = stub.Detect(read_test_file_pcm(testfile))
        for seg in outputs:
            print(seg.start, seg.end)
            pcmwav = np.concatenate((pcmwav, wav[seg.start:seg.end]))
        write('wavwav.wav', 16000, wavwav)
        write('pcmwav.wav', 16000, pcmwav)
