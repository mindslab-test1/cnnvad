import argparse
from concurrent import futures
import logging
#Grpc
import grpc
import vad_pb2_grpc
from vad_pb2 import WavFileBinary, Segment, Speech
#Engine
import pack_model

CHUNK_SIZE = 1024 * 1024  #1MB


class VADServicer(vad_pb2_grpc.VadServicer):
    def __init__(self, args):
        self.pack = pack_model.packed(args.ckpt, args.device, args.thres, None)
        self.pcm_duration = args.duration #1024*this sample
        self.ignorance = args.ignorance
    def DetectWav(self, request_iterator, context):
        logging.info('Start VAD WAV')
        try:
            wav = self.pack.BytesWrapper(request_iterator)
            result_tf = self.pack.VADetection(wav)
            start = -1
            end = 0
            last_value = 0
            zero_count = 0
            logging.debug(result_tf)
            for i,v in enumerate(result_tf):
                if v:
                    start = start if start>=0 else  i*1024
                    end = (i+1)*1024
                    zero_count=0
                else:
                    zero_count+=1
                    if zero_count>self.ignorance:
                        if start>=0:
                            logging.debug('response: {start: %s, end: %s}', start, end)
                            yield Segment(start = start, end = end)
                            start = -1
                        zero_count=0
                last_value = v
            if start>=0:
                logging.debug('response: {start: %s, end: %s}', start, end)
                yield Segment(start = start, end = end)
            logging.info('End VAD WAV')
            del wav
            del result_tf
            self.pack.EmptyCache()
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def Detect(self, request_iterator, context):
        logging.info('Start VAD PCM')
        try:
            start =-1
            end = 0
            last_value = 0
            zero_count=0
            result_tf = []
            for i,v in enumerate(self.pack.VADGenerator(
                self.pack.PCMWrapper(request_iterator, 
                    1024*self.pcm_duration))):
                for j,vv in enumerate(v):
                    if vv:
                        start = start if start>=0 else (self.pcm_duration*i+j)*1024 
                        end = (self.pcm_duration*i+j+1)*1024
                        zero_count = 0
                    else:
                        zero_count+=1
                        if zero_count>self.ignorance:
                            if start>=0:
                                logging.debug('response: {start: %s, end: %s}', start, end)
                                yield Segment(start = start, end = end)
                            zero_count =0
                            start =-1
                    last_value = vv
                result_tf.append(v)
            if start>=0:
                logging.debug('response: {start: %s, end: %s}', start, end)
                yield Segment(start = start, end = end)
            logging.debug(result_tf) 
            logging.info('End VAD PCM')
            del result_tf
            self.pack.EmptyCache()
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))



def serve(args):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers = args.max_workers))
    vad_pb2_grpc.add_VadServicer_to_server(VADServicer(args), server)
    logging.info('Starting server. Listening on port %d', args.port)

    server.add_insecure_port(f'[::]:{args.port}')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--device', type=int, default=-1, help="GPU device, -1 stands for CPU")
    parser.add_argument('-p', '--port', type=int, default=5002, help="port")
    parser.add_argument('-t',
                        '--thres',
                        type=float,
                        default=0.2,
                        help="threshold")
    parser.add_argument('-c',
                        '--ckpt',
                        type=str,
                        default='checkpoint/vad_08_24_15_epoch=3557.ckpt',
                        help='checkpoint')
    parser.add_argument('-u',
                        '--duration',
                        type=int,
                        default=16,
                        help='frame duration * 1024 sample')
    parser.add_argument('-i',
                        '--ignorance',
                        type=int,
                        default=1,
                        help='ignore i continuous false frames')
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-w', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='max workers',
                        type=int,
                        default=30)
    args = parser.parse_args()
    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    serve(args)
