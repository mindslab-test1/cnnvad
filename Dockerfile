From nvcr.io/nvidia/pytorch:19.06-py3

Expose 5002

Run apt-get update && apt-get install -y \
    software-properties-common
Run add-apt-repository universe
Run apt-get update && apt-get install -y \
    curl \
    git \
    ffmpeg \
    libjpeg-dev \
    libpng-dev
Run pip install --upgrade pip
Run pip install torch==1.4.0
Run pip install torchvision==0.5.0
Run pip install torchaudio==0.4.0
Run pip install omegaconf==2.0.0

WORKDIR /root/workspace
COPY requirements.txt /root/workspace/
Run pip install -r requirements.txt

Run ldconfig && \
apt-get clean && \
apt-get autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/*

COPY *.py /root/workspace/
COPY *.proto /root/workspace/
COPY checkpoint /root/workspace/checkpoint
COPY goodsen1_mono.wav /root/workspace
Run python3 -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./vad.proto

ENTRYPOINT ["python3" , "server.py"]
