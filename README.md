# CNNVAD
vad
voice activity detection

## Setup
1. `pip install -r requirements.txt`
1. `python -m grpc.tools.protoc -I. --python_out=. --grpc_python_out=. cnnvad.proto`

## Run Server
1. `python server.py -d {device=1} -p {port=5002} -t {threshold=0.2}`

## Run Client
1. `python client.py `

Written by 이준혁
