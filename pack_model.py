import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from scipy.io.wavfile import read
import io
import librosa

class Swish(nn.Module):
    def __init__(self):
        super(Swish, self).__init__()
        self.sig = nn.Sigmoid()

    def forward(self, x):
        x = self.sig(x) * x
        return x

class ResBlock(nn.Module):
    def __init__(self, in_c, out_c, kernel, pad, stride, act):
        super(ResBlock, self).__init__()
        self.in_c = in_c
        self.out_c = out_c
        self.act = act
        self.pad = pad
        self.stride = stride
        self.kernel = kernel
        self.conv1d1 = nn.Conv1d(in_c,
                                 out_c,
                                 self.kernel,
                                 padding=self.pad,
                                 stride=self.stride,
                                 bias=False)
        self.conv1d2 = nn.Conv1d(out_c,
                                 out_c,
                                 self.kernel,
                                 padding=self.pad,
                                 stride=self.stride,
                                 bias=False)
        self.bn1 = nn.BatchNorm1d(in_c)
        self.bn2 = nn.BatchNorm1d(out_c)
        self.act = act
        self.scale = nn.Identity() if self.stride == 1 else nn.Conv1d(
            in_c, out_c, 1, stride=stride * stride, bias=False)

    def forward(self, x):
        x = self.bn1(x)
        x = self.act(x)
        prev = x
        x = self.conv1d1(x)
        x = self.bn2(x)
        x = self.act(x)
        x = self.conv1d2(x) + self.scale(prev)
        return x


class packed(object):
    def __init__(self, ckpt_path, device, thres, logger):
        self.device = device if device !=-1 else 'cpu'
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
        self.model = VAD(ckpt_path).to(self.device)
        self.model.eval()
        self.sr = 16000
        self.thres = thres
        self.EmptyCache()

    def BytesWrapper(self, wav_binary_iterator):
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
        with torch.no_grad():
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)

            wav = io.BytesIO(wav)
            sampling_rate, wav = read(wav)
            if len(wav.shape) == 2:
                wav = wav[:, 0]

            if wav.dtype == np.int16:
                wav = wav / 32768.0
            elif wav.dtype == np.int32:
                wav = wav / 2147483648.0
            elif wav.dtype == np.uint8:
                wav = (wav - 128) / 128.0

            wav = wav.astype(np.float32)
            if sampling_rate != self.sr:
                wav = librosa.resample(wav, sampling_rate, self.sr)
                wav = np.clip(wav, -1.0, 1.0)
        return wav

    def PCMWrapper(self, wav_binary_iterator, duration = 1024*16):
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
        with torch.no_grad():
            #16kHz, 16bit-d
            offset = 0
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)
                while offset +2*duration <len(wav):
                    npwav = np.frombuffer(wav[offset:offset+2*duration], dtype = 'int16')
                    npwav = npwav / 32768.0
                    npwav = npwav.astype(np.float32)
                    offset += 2*duration
                    yield npwav
            if offset<len(wav):
                npwav = np.frombuffer(wav[offset:offset+2*duration], dtype = 'int16')
                npwav = npwav / 32768.0
                npwav = npwav.astype(np.float32)
                offset += 2*duration
                yield npwav

    def VADetection(self, wav):
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
        wav = torch.tensor(wav).to(self.device)
        x = self.model(wav)
        result = [int(i.item()) for i in (x > self.thres)]
        del x
        del wav
        return result

    def VADGenerator(self, wav):
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
        for w in wav:
            w = torch.tensor(w).to(self.device)
            x = self.model(w)
            result = [int(i.item()) for i in (x > self.thres)]
            del x
            del w
            yield result
    def EmptyCache(self):
        if self.device != 'cpu':
            torch.cuda.set_device(self.device)
            torch.cuda.empty_cache()
        return


class VAD(nn.Module):
    def __init__(self, ckpt_path):
        super(VAD, self).__init__()
        self.stem = nn.Sequential(
            nn.Conv1d(1, 32, 5, padding=2, stride=2),  #/2
            nn.BatchNorm1d(32),
            Swish(),
            nn.Conv1d(32, 32, 5, padding=2, stride=2))  #/4
        self.res = nn.Sequential(
            ResBlock(32, 32, 5, 2, 2, Swish()),  # /16 
            ResBlock(32, 32, 5, 2, 2, Swish()),  # /64
            ResBlock(32, 32, 5, 2, 2, Swish()),  # /256
            ResBlock(32, 32, 5, 2, 2, Swish()),  # /1024
            ResBlock(32, 32, 5, 2, 1, Swish()),  # /1024
            nn.BatchNorm1d(32),
            Swish(),
            nn.Conv1d(32, 1, 1),
            nn.Flatten(),
            nn.Sigmoid())
        _ckpt = torch.load(ckpt_path, map_location = torch.device('cpu'))
        self.load_state_dict(_ckpt['state_dict'])
        del _ckpt

    def forward(self, x):  #x: [B, T]
        x = x.unsqueeze(0).unsqueeze(0)
        x = self.stem(x)
        x = self.res(x)
        #x = self.conv(x)  #x: [B, T/1024]
        x = x.squeeze(0)
        return x
